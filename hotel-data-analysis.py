# -*- coding:utf-8 -*-
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt



def sample(x,y):
    plt.rcParams['figure.figsize'] = (102.4,76.8)
    plt.plot(x,y)
    plt.xticks(rotation=90)
    plt.show()

def line_chart():

    df = pd.read_csv('data.csv',header=None)
    # data = np.array(df.loc[:,:])
    dat = list(df.loc[:,0])
    values = list(df.loc[:,1])
    # print(dat)
    # print(values)
    x = dat[1:]
    y = values[1:]

    #设置图片大小
    plt.rcParams['lines.linewidth'] = 1
    plt.rcParams['lines.linestyle'] = '-'
    plt.rcParams['figure.figsize'] = (10.24,7.68)
    # "r" 表示红色，ms用来设置*的大小
    plt.plot(x, y, "r", marker='*', ms=10, label="a")
    # plt.plot([1, 2, 3, 4], [20, 30, 80, 40], label="b")
    plt.xticks(rotation=45)
    plt.xlabel("日期")
    plt.ylabel("营业额")
    plt.title("饭店日常消费情况")
    # 在折线图上显示具体数值, ha参数控制水平对齐方式, va控制垂直对齐方式
    for x1, y1 in zip(x, y):
        plt.text(x1, int(y1) + 1, str(y1), ha='center', va='bottom', fontsize=20, rotation=0)

    plt.xticks(rotation=90)

    plt.savefig("first.jpg")
    plt.show()

def columnar_chart():
    df = pd.read_csv('data.csv',index_col='date')
    # print(df.index)
    df.index = pd.to_datetime(df.index)
    # print(df['2019-01-01':'2019-01-31'])
    month = df.resample('M').sum()
    # print(df.resample('M').sum())
    # print(type(month))
    month_date = ["-".join(str(i).split('-')[:2]) for i in month.index.values]
    month_sales = [i[0] for i in month.values]
    # print(month_date)
    # print(month_sales)
    x = month_date
    y = month_sales
    # 绘图
    plt.bar(x=x, height=y, label='Month-Sales', color='steelblue', alpha=0.8)
    # 在柱状图上显示具体数值, ha参数控制水平对齐方式, va控制垂直对齐方式
    for x1, yy in zip(x, y):
        plt.text(x1, yy + 1, str(yy), ha='center', va='bottom', fontsize=20, rotation=0)
    # 设置标题
    plt.title("饭店月度销售额")
    # 为两条坐标轴设置名称
    plt.xlabel("月份")
    plt.ylabel("销售额")
    plt.xticks(rotation=90)
    # 显示图例
    plt.legend()
    plt.savefig("second.jpg")
    plt.show()

def pie_chart():
    df = pd.read_csv('data.csv',index_col='date')
    # print(df.index)
    df.index = pd.to_datetime(df.index)
    # print(df['2019-01-01':'2019-01-31'])
    quater = df.resample('Q').sum()
    # print(df.resample('M').sum())
    # print(type(month))
    quater_date = ["-".join(str(i).split('-')[:2]) for i in quater.index.values]
    quater_sales = [i[0] for i in quater.values]
    # print(quater_date)
    # print(quater_sales)

    plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签

    labels = ['Q1', 'Q2', 'Q3', 'Q4']
    # sizes = [2, 5, 12, 70, 2, 9]
    explode = (0, 0, 0, 0.1, 0, 0)
    # plt.pie(quater_sales, explode=explode, labels=labels, autopct='%1.1f%%', shadow=False, startangle=150)
    plt.pie(quater_sales, labels=labels, autopct='%1.1f%%', shadow=False, startangle=150)
    plt.title("饭店季度收入展示")
    plt.legend()
    plt.savefig("third.jpg")
    plt.show()

def main():
    # 处理乱码
    matplotlib.rcParams['font.sans-serif'] = ['SimHei']  # 用黑体显示中文
    line_chart()
    columnar_chart()
    pie_chart()


if __name__ == '__main__':
    main()