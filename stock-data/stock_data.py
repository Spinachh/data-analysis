# !/usr/bin/python
# -*-coding:utf-8-*-
# Author：
# Date：2020/6/16

import baostock as bs
import pandas as pd
from pandas import read_csv
import matplotlib.pyplot as plt
import mplfinance as mpf

def get_data():
    #### 登陆系统 ####
    lg = bs.login(user_id="anonymous", password="123456")
    # 显示登陆返回信息
    print('login respond error_code:' + lg.error_code)
    print('login respond  error_msg:' + lg.error_msg)
    # 参数说明：
    #     参数名称	参数描述
    #     date	交易所行情日期
    #     code	证券代码
    #     open	开盘价
    #     high	最高价
    #     low	最低价
    #     close	收盘价
    #     preclose	昨日收盘价
    #     volume	成交量（累计 单位：股）
    #     amount	成交额（单位：人民币元）
    #     adjustflag	复权状态(1：后复权， 2：前复权，3：不复权）
    #     turn	换手率
    #     tradestatus	交易状态(1：正常交易 0：停牌）
    #     pctChg	涨跌幅
    #     peTTM	动态市盈率
    #     pbMRQ	市净率
    #     psTTM	市销率
    #     pcfNcfTTM	市现率
    #     isST	是否ST股，1是，0否
    #### 获取历史K线数据 ####
    # 获取历史A股K线数据：query_history_k_data()
    # code:股票代码，start：开始日期，end：结束日期时间格式为：YYYY-MM-DD”
    # requency：数据类型，默认为d，日k线；d = 日k线、w = 周、m = 月、5 = 5分钟、15 = 15分钟、
    # 30 = 30分钟、60 = 60分钟k线数据，不区分大小写；周线每周最后一个交易日才可以获取，月线第月最后一个交易日才可以获取。
    rs = bs.query_history_k_data("sh.600000",
                                 "Date,code,"
                                 "open,"
                                 "high,"
                                 "low,"
                                 "close,"
                                 "preclose,"
                                 "volume,"
                                 "amount,"
                                 "adjustflag,"
                                 "turn,"
                                 "tradestatus,"
                                 "pctChg,"
                                 "peTTM,"
                                 "pbMRQ,"
                                 "psTTM,"
                                 "pcfNcfTTM,"
                                 "isST",
                                 start_date='2020-01-01',
                                 end_date='2020-6-15',
                                 frequency="d",
                                 adjustflag="3")  # frequency="d"取日k线，adjustflag="3"默认不复权
    print('query_history_k_data respond error_code:' + rs.error_code)
    print('query_history_k_data respond  error_msg:' + rs.error_msg)

    #### 打印结果集 ####
    data_list = []
    while (rs.error_code == '0') & rs.next():
        # 获取一条记录，将记录合并在一起
        data_list.append(rs.get_row_data())
    result = pd.DataFrame(data_list, columns=rs.fields)
    #### 结果集输出到csv文件 ####
    result.to_csv("history_data2.csv", encoding="gbk", index=False)
    # print(result)

def drawl_volume(stock_data):
    # print(stock_data.info())

    #绘制成交量
    stock_data['volume'].plot(grid=True,color='red',label='600000.SH')
    plt.title('2020-01~2020-06 volume', fontsize='9')
    plt.ylabel('volume', fontsize='8')
    plt.xlabel('Date', fontsize='8')
    plt.legend(loc='best',fontsize='small')
    plt.show()

def drawl_times_pickle(stock_data):

    stock_data.index.name = 'Date'  # 日期为索引列
    data = stock_data.loc['2020-01-01':'2020-06-01']  # 获取某个时间段内的时间序列数据
    data[['close', 'volume']].plot(secondary_y='volume', grid=True)
    plt.title('2020-1~2020-6 close and volume', fontsize='9')
    plt.show()


def drawl_candle():
    stock_data = read_csv('history_data.csv',parse_dates=['Date'],index_col='Date')
    data = stock_data.loc['2020-01-02':'2020-02-01']  # 获取某个时间段内的时间序列数据
    # print(data)
    #绘制一条均线
    # mpf.plot(data, type='candle',mav=(2, 5, 10))
    mpf.plot(data, type='candle',mav=(2, 5, 10),volume=True)

def main():

    # get_data()    #获取数据函数
    stock_data = read_csv('history_data2.csv',parse_dates=['Date'],index_col='Date')
    stock_data.index.name = 'Date'
    stock_data = stock_data.sort_values(by='Date')
    drawl_volume(stock_data)
    drawl_times_pickle(stock_data)
    drawl_candle()

if __name__ == '__main__':
    main()