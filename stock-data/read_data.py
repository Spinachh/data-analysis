# !/usr/bin/python
# -*-coding:utf-8-*-
# Author：
# Date：2020/6/16

# """
# from pandas import read_csv
# import matplotlib.pyplot as plt
# stock_data = read_csv('history_data2.csv',parse_dates=['date'],index_col='date')
# stock_data.index.name = 'date'
# stock_data = stock_data.sort_values(by='date')
# # print(stock_data.info())
#
#
# #绘制成交量
# stock_data['volume'].plot(grid=True,color='red',label='600000.SH')
# plt.title('2020-01~2020-06 volume', fontsize='9')
# plt.ylabel('volume', fontsize='8')
# plt.xlabel('date', fontsize='8')
# plt.legend(loc='best',fontsize='small')
# plt.show()
#
# #绘制时间序列数据
# stock_data.index.name = 'date'  # 日期为索引列
# data = stock_data.loc['2020-01-02':'2020-06-01']  # 获取某个时间段内的时间序列数据
# data[['close', 'volume']].plot(secondary_y='volume', grid=True)
# plt.title('2020-1~2020-6 close and volume', fontsize='9')
# plt.show()
# """

#绘制Candle线图
from pandas import read_csv
import mplfinance as mpf

stock_data = read_csv('history_data.csv',parse_dates=['Date'],index_col='Date')
stock_data.index.name = 'Date'  # 日期为索引列
stock_data = stock_data.sort_values(by='Date')
data = stock_data.loc['2020-01-02':'2020-02-01']  # 获取某个时间段内的时间序列数据
# print(data)

mpf.plot(data,type='candle',mav=(2,5,10))


