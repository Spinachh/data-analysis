import pandas as pd
from pyecharts import Map
from collections import Counter


def add_map(attr,value):
    map0 = Map("Covid-19-分离菌株数量", width=1200, height=900)
    map0.add("世界地图", attr, value, maptype="world",visual_range=[0,100], is_visualmap=True, visual_text_color='#000')
    map0.render(path="世界地图.html")

def read_csv():

    df = pd.read_csv('covid-19.csv')
    # print(df.head())
    country_col = df.iloc[:,4].values   #宿主
    country_col = df.iloc[:,5].values   #国家

    # print(type(country_col),list(country_col))
    return list(country_col)

def get_count_by_count(l):
    dict = {}
    content = []
    # print(l)
    for j in l:
        try:
            if ':' in j:
                j = j.split(':')[0]
                content.append(j)
        except:
            print('此处的值有缺失')
    # print(content)
    s = set(content) #集合：得到不重复的元素
    for i in s:
        dict[i] = l.count(i) #对集合中每个元素分别计数，存入dictionary中

    return dict

def main():
    content = read_csv()
    content_tict = get_count_by_count(content)
    # print(content_tict)
    content_tict["United States"] = content_tict.pop("USA")
    attr = content_tict.keys()
    value = content_tict.values()
    # print(attr,value)
    add_map(attr,value)

if __name__ == '__main__':
    main()